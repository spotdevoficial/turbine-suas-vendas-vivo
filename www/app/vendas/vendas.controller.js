	/************************
	 *
	 *  Autor: Swellit Solutions
	 *  Data: 13/11/2015
	 *  Função: Controller das vendas da aplicação
	 *
	 ************************/

	 angular.module('starter.vendasController', [])

	 .controller('vendasController', function($scope, $state, $rootScope, $ionicPopup, $ionicLoading, $cordovaSQLite,listPlanos, listVendas, sendVenda, $filter) {

	 	$rootScope.showBrandLogo = $state.current.name == "app.home";

	 	$scope.venda = {};
	 	$scope.planos = [];

	 	$scope.venda.dataRegistro = $filter("date")(Date.now(), 'dd/MM/yyyy');

	 	$scope.getDados = function (){

	 		listPlanos(
	 			function (response){
	 				$scope.planos = response.data.data;
	 				var query = "DELETE FROM planos WHERE 1";
	 				$cordovaSQLite.execute(db, query);

	 				for(plano in $scope.planos){
	 					var query = "INSERT INTO planos (idPlano, label) VALUES (?,?)";
	 					$cordovaSQLite.execute(db, query, [$scope.planos[plano].id, $scope.planos[plano].label])
	 					.then(function(res) {
	 						console.log("INSERT ID -> " + res.insertId);
	 					}, function (err) {
	 						console.error(err);
	 					});
	 				}
					//add in table sqlite
					console.log($scope.planos);
				},
				function (response){
					var query = "SELECT * FROM planos WHERE 1";
					$cordovaSQLite.execute(db, query).then(function(res) {
						for(plano in res.rows){
							$scope.planos.push({'id':res.rows[plano].idPlano, 'label':res.rows[plano].label});
						}
					}, function (err) {
						console.error(err);
					});
					//joga os dados do sqlite para planos
				}
				);
	 	}

	 	$scope.enviaNovaVenda = function (){
	 		var date = $scope.venda.dataRegistro.split('/');
	 		var dateTime = new Date(date[2], date[1], date[0]);


	 		if (dateTime < new Date()) {
                $rootScope.likeIcon = false;
	 			$ionicPopup.alert({
							title: 'Ops!',
							template: 'Data inválida.'
						});
	 			return;
	 		}
	 		if($scope.venda.linha.length < 13){
                console.log($scope.venda.linha.length);
                $rootScope.likeIcon = false;
				$ionicPopup.alert({
						title: 'Ops!',
						template: 'Linha inválida.'
					});
	 			return;	 		
	 		}
	 		if($scope.venda.idPlano == undefined){
                $rootScope.likeIcon = false;
 				$ionicPopup.alert({
						title: 'Ops!',
						template: 'Selecione um plano.'
					});
	 			return;
	 		}

	 		$scope.venda.CPF = window.localStorage.userCpf;

	 		sendVenda($scope.venda, function (response){

 				if (response.data.status.erro) {
                    //caso não aja dados cadastrados
                    $rootScope.likeIcon = false;
                    $ionicPopup.alert({
                        title: 'Ops',
                        template: 'Parece tem algo errado, verifique e tente novamente'
                    });
				} else {
                    $rootScope.likeIcon = true;
					$ionicPopup.alert({
						title: 'Sucesso',
						template: response.data.status.mensagem
					});
					$state.go('app.home');
				}
			},

			function (response){
				var query = "INSERT INTO vendas (cpf, idPlano, dataRegistro, linha) VALUES (?,?,?,?)";
				$cordovaSQLite.execute(db, query, [$scope.venda.CPF, $scope.venda.idPlano, $scope.venda.dataRegistro, $scope.venda.linha])
				.then(function(res) {
					console.log("INSERT ID -> " + res.insertId);
				}, function (err) {
					console.error(err);
				});
                $rootScope.likeIcon = false;
				$ionicPopup.alert({
					title: 'Ops',
					template: 'Parece que você está sem internet, armazenamos sua venda, sincronize assim que possível'
				});

				$state.go('app.home');
			});
		}


		$scope.listarVendas = function(){

			$ionicLoading.show({
	            template: 'Baixando lista de vendas...'
	        });
			listVendas(window.localStorage.userCpf, function (response){
				if (response.data.status.erro) {
					//caso não aja dados cadastrados
					$ionicLoading.hide();
                    $rootScope.likeIcon = false;
					$ionicPopup.alert({
						title: 'Ops',
						template: 'Parece tem algo errado, verifique e tente novamente'
					});
					$state.go('app.home');
				} else {
					console.log(response.data);
					$scope.vendasList = response.data.data;
					$ionicLoading.hide();
				}
			},
			function (response){
				$ionicLoading.hide();
                $rootScope.likeIcon = false;
				$ionicPopup.alert({
					title: 'Ops',
					template: 'Parece que ocorreu um erro na comunicação, verifique e tente novamente'
				});
				$state.go('app.home');
			});
		}




		$scope.vendaNaoCadastrada = function (){

			$state.go('app.venda_NaoCadastrado');
		}



	 	$scope.enviaNovaVendaNaoCadastrado = function (){
	 		var date = $scope.venda.dataRegistro.split('/');
	 		var dateTime = new Date(date[2], date[1], date[0]);


	 		if (dateTime < new Date()) {
                $rootScope.likeIcon = false;
	 			$ionicPopup.alert({
                    title: 'Ops!',
                    template: 'Data inválida.'
                });
	 			return;
	 		}
	 		if($scope.venda.linha.length < 13){
                console.log($scope.venda.linha.length);
                $rootScope.likeIcon = false;
				$ionicPopup.alert({
                    title: 'Ops!',
                    template: 'Linha inválida.'
                });
	 			return;	 		
	 		}
	 		if($scope.venda.idPlano == undefined){
                $rootScope.likeIcon = false;
 				$ionicPopup.alert({
						title: 'Ops!',
						template: 'Selecione um plano.'
					});
	 			return;
	 		}


	 		sendVenda($scope.venda, function (response){

 				if (response.data.status.erro) {
                    //caso não aja dados cadastrados
                    $rootScope.likeIcon = false;
                    $ionicPopup.alert({
                        title: 'Ops',
                        template: 'Parece tem algo errado, verifique e tente novamente'
                    });
				} else {
                    $rootScope.likeIcon = true;
					$ionicPopup.alert({
						title: 'Sucesso',
						template: response.data.status.mensagem
					});
					$state.go('app.login');
				}
			},

			function (response){
				var query = "INSERT INTO vendas (cpf, idPlano, dataRegistro, linha) VALUES (?,?,?,?)";
				$cordovaSQLite.execute(db, query, [$scope.venda.CPF, $scope.venda.idPlano, $scope.venda.dataRegistro, $scope.venda.linha])
				.then(function(res) {
					console.log("INSERT ID -> " + res.insertId);
				}, function (err) {
					console.error(err);
				});
                $rootScope.likeIcon = false;
				$ionicPopup.alert({
					title: 'Ops',
					template: 'Parece que você está sem internet, armazenamos sua venda, sincronize assim que possível'
				});

				$state.go('app.login');
			});
		}


		$scope.getDDD = function (linha){
			console.log(linha);
			return linha.toString().substring(0,2); 
		}


		$scope.removeDDD = function(linha){
			return linha.toString().substring(2, 11);
		}


});

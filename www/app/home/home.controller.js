/************************
 *
 *  Autor: Swellit Solutions
 *  Data: 10/11/2015
 *  Função: Controller da tela inicial da aplicação
 *
 ************************/

angular.module('starter.homeController', [])

.controller('homeController', function ($scope, $state, $rootScope, $cordovaSQLite, $ionicLoading, $ionicPopup, sendVenda) {

    $rootScope.$on('$stateChangeStart', 
    function(event, toState, toParams, fromState, fromParams){ 
        // Mostra a logo na navbar apenas se for a navbar da home
        $rootScope.showBrandLogo = toState.name == "app.home";
    
    })


    // Mostra a logo na navbar apenas se for a navbar da home
    $rootScope.removeNavbarBackground = $state.current.name == "app.home";
    console.log($rootScope.removeNavbarBackground);

    $scope.errorSync = false;

    $scope.novaVenda = function () {
        $state.go('app.cadastrar_novaVenda');
    }
    $scope.listaVenda = function () {
        $state.go('app.listar_Vendas');
    }
    $scope.extratoPontos = function (){
        $state.go('app.extrato_Pontos');
    }
    $scope.syncDB = function () {
        $ionicLoading.show({
            template: 'Sincronizando, aguarde...'
        });
        var query = "SELECT * FROM vendas WHERE 1";
        $cordovaSQLite.execute(db, query)
            .then(
                function (res) {
                    for (b = 0; b < res.rows.length; b++) {
                        var vendasToSync = {
                            'idPlano': res.rows[b].idPlano,
                            'CPF': res.rows[b].cpf,
                            'dataRegistro': res.rows[b].dataRegistro,
                            'linha': res.rows[b].linha,
                            'idGC': res.rows[b].idGC
                        };
                        c = b;
                        console.log(vendasToSync);
                        sendVenda( vendasToSync, function (response) {
                                if (response.data.status.erro) {
                                    //caso não aja dados cadastrados
                                    $scope.errorSync = true;
                                    console.log('invalido');
                                } else {
                                    var query = "DELETE FROM vendas WHERE id = " + res.rows[c].id;
                                    console.log(query);
                                    console.log($cordovaSQLite);
                                    $cordovaSQLite.execute(db, query)
                                        .then(
                                            function (res) {

                                            },
                                            function (err) {
                                                console.error(err);
                                            });
                                }
                                $ionicLoading.hide();
                            },
                            function error(response) {
                                $ionicLoading.hide();
                                $rootScope.likeIcon = false;
                                $ionicPopup.alert({
                                    title: 'Ops',
                                    template: 'Parece que você está sem internet, armazenamos sua venda, sincronize assim que possível'
                                });
                                b = res.rows.length;

                            }
                        );
                    }
                    $ionicLoading.hide();
                },
                function (err) {
                    console.error(err);
                    $ionicLoading.hide();
                }
            , function (err) {
                console.error(err);
                $ionicLoading.hide();
            });

        if(!$scope.errorSync){
            $rootScope.likeIcon = true;
            $ionicPopup.alert({
                title: 'Sucesso!',
                template: 'Todos os dados foram sincronizados.'
            });
        }else{
            $rootScope.likeIcon = false;
            $ionicPopup.alert({
                title: 'Ops!',
                template: 'Parece que algo deu errado, tente novamente.'
            });
        }

    }
});

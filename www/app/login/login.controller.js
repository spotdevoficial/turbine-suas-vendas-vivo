/************************
 *
 *  Autor: Swellit Solutions
 *  Data: 10/11/2015
 *  Função: Controller da tela de login
 *
 ************************/
angular.module('starter.loginController', ['starter.vivoService', 'starter.util'])

.controller('loginController', function($scope, login, $ionicPopup, $state, $ionicHistory, validaCPF, $rootScope) {

    // Armazena os dados de login do usuário encaminhado pelo form
    $scope.loginData = {};
    $scope.remember = {value:true};

    //metodo para o primeiro login
    $scope.doLogin = function() {

        if($scope.loginData.cpf == null){ //verifica cpf vazio
            $rootScope.likeIcon = false;
            $ionicPopup.alert({
                title: 'Ops',
                template: 'Preencha todos os campos corretamente',
                okText: 'Prosseguir'
            });
            return;
        }

        if (validaCPF($scope.loginData.cpf)) {// valida cpf
            login($scope.loginData,
                function(response) {
                    if (response.data.status.erro) { //caso o servidor retorne algum erro
                        $rootScope.likeIcon = false;
                        $ionicPopup.alert({
                            title: 'Ops!',
                            template: 'Usuário ou senha inválidos.'
                        });
                    } else {
                        window.localStorage.userCpf = response.data.data.CPF;
                        window.localStorage.nomeCG = response.data.data.nomeGC; //armazeno esse dado para exibir na ajuda
                        window.localStorage.celularGC = response.data.data.celularGC; //armazeno esse dado para exibir na ajuda
                        
                        window.localStorage.userLogado = JSON.stringify(response.data.data);
                        $ionicHistory.nextViewOptions({ //caso não vai para a view home
                            disableBack: true
                        });
                        $state.go('app.home');
                    }
                },
                function(response) {
                    //caso aja erro na comunicação com servidor
                    $rootScope.likeIcon = false;
                    $ionicPopup.alert({
                            title: 'Ops!',
                            template: 'Parece que ocorreu um erro na comunicação, verifique e tente novamente'
                        });

                }, $scope.remember.value); //caso o valor do remember seja true, sera armazenado na storage pelo proprio metodo "login"


        } else {
            $rootScope.likeIcon = false;
            $ionicPopup.alert({ //caso o cpf seja invalido
                title: 'Ops!',
                template: 'Usuário ou senha inválidos.'
            });
        }
    }


    // Evento de quando o botão "Não sou cadastrado" é pressionado
    $scope.onClickCadastrar = function() {

        $state.go('app.cadastrar');

    }

    $scope.onClickEsqueciSenha = function() {

        $state.go('app.resgatar_senha');

    }

    // Autentica o usuario
    $scope.auth = function (){// verifica se esta com a storage setada antes de chamar

    	if(!typeof(window.localStorage.auth) == undefined){
	    	if(!JSON.parse(window.localStorage.auth) == null){
		    	login(
		    		JSON.parse(window.localStorage.auth),
		    		function (response){
		    			$ionicHistory.nextViewOptions({ //caso não vai para a view home
	                        disableBack: true
	                    });
                        
	                    $state.go('app.home');
		    		},
		    		function (response){
                        $rootScope.likeIcon = false;
		    			$ionicPopup.alert({
	                        title: 'Ops',
	                        template: 'Parece que ocorreu um erro na comunicação, verifique e tente novamente'
	                    });
		    		}
		    	);
	   		}
	   	}
    }
});

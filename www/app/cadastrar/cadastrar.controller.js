/************************
 *
 *  Autor: Swellit Solutions
 *  Data: 10/11/2015
 *  Função: Controller da tela inicial da aplicação
 *
 ************************/

angular.module('starter.cadastrarController', [])

.controller('cadastrarController', function($scope, $ionicModal, $timeout, checkCPF, $state, $ionicPopup, validaCPF, updatePassword, $rootScope) {

	$scope.dados = {};
	$scope.regulamento = {value:false};
	$scope.cadastro = {};
	$scope.verificaCPF = function (){

		if(window.localStorage.userCpf && $scope.cadastro == undefined){
			$scope.cadastro.cpf = window.localStorage.userCpf;
			$scope.btnMSG = "ALTERAR SENHA";
		}
		if(validaCPF($scope.cadastro.cpf)){
			checkCPF(
				$scope.cadastro.cpf,
				function (response){
					if (response.data.status.erro) {

						if(!response.data.status.mensagem == "Usuário já cadastrado!"){
						$ionicPopup.show({
                       		title: 'Ops!',
                        	template: response.data.status.mensagem,
                        	buttons: [{ // Array[Object] (optional). Buttons to place in the popup footer.
							    text: 'Voltar',
							    type: 'button-default',
							    onTap: function(e) {
									$state.go('app.login');
							    }
							  }, 
							  {
							    text: 'Prosseguir mesmo assim',
							    type: 'button-positive',
							    onTap: function(e) {
									$state.go('app.venda_NaoCadastrado');
							    }
						  	}]
	                	});
					}else{
						$ionicPopup.show({
                       		title: 'Ops!',
                        	template: response.data.status.mensagem,
                        	buttons: [{ // Array[Object] (optional). Buttons to place in the popup footer.
							    text: 'Rertonar ao login',
							    type: 'button-default',
							    onTap: function(e) {
									$state.go('app.login');
							    }
							  }]
	                	});

					}

                    } else {
                        // ccaso aja dados cadastrados
                        $scope.exibirCPF(response.data.data);
                    }
				},
				function (response){
                    $rootScope.likeIcon = false;
				 	$ionicPopup.alert({
                        title: 'Ops',
                        template: 'Parece que ocorreu um erro na comunicação, verifique e tente novamente'
	                });
				}
			);
		}else{
            $rootScope.likeIcon = false;
            $ionicPopup.alert({ //caso o cpf seja invalido
                title: 'Ops!',
                template: 'CPF inválido.'
            });
        }
	}
	$scope.exibirCPF = function (dados){
		window.localStorage.userView = JSON.stringify(dados);
	    $state.go('app.cadastrar_exibirCpf');
	}



	$scope.getDados = function (){
		
			$scope.regulamento = {value:false};
			$scope.termos = true;
			$scope.btnMSG = "AVANÇAR";
			$scope.msgUpdate = {};
			$scope.msgUpdate.titulo = "Parabéns!";
			$scope.msgUpdate.texto = "Você ganhou 50 pontos pelo seu cadastro!";
		
		$scope.dados = JSON.parse(window.localStorage.userView);
		console.log($scope.dados);
		window.localStorage.userView = null;
	}


	$scope.setDataUpdateSenha = function (){
		window.localStorage.userView = JSON.stringify($scope.dados);
		$state.go('app.update_SenhaCadastrado');
	}

	$scope.sendUpdateSenha = function (){

		if(!$scope.regulamento.value){
            $rootScope.likeIcon = false;
			$ionicPopup.alert({
                    title: 'Ops!',
                    template: 'Você precisa concordar com o regulamento.'
                });
			return;
		}

        if($scope.dados.senha == undefined){
            $rootScope.likeIcon = false;
           $ionicPopup.alert({
                    title: 'Ops!',
                    template: 'Preencha corretamente os campos das senha!.'
                });
			return;
        }

		if ($scope.dados.senha != $scope.dados.repSenha) {
            $rootScope.likeIcon = false;
			$ionicPopup.alert({
                    title: 'Ops!',
                    template: 'As senhas não conferem.'
                });
			return;
		}

		updatePassword(
			$scope.dados,
			function (response){
				if (response.data.status.erro) {
						//caso não aja dados cadastrados
                        $rootScope.likeIcon = false;
						$ionicPopup.alert({
                            title: 'Ops',
                            template: 'Parece tem algo errado, verifique e tente novamente'
                		});
                    } else {
                        $rootScope.likeIcon = true;
                        $ionicPopup.alert({
                            title: $scope.msgUpdate.titulo,
                            template: $scope.msgUpdate.texto,
                            okText: 'Fazer login'
                		});
                        
                        if(window.localStorage.userCpf){
                        	window.localStorage.clear();
                        }
                        $state.go('app.login');

                    }
			},
			function (response){
                $rootScope.likeIcon = false;
				$ionicPopup.alert({
	            	title: 'Ops',
	           	 	template: 'Parece que ocorreu algo errado na comunicação, tente de novo mais tarde.'
	        	});

			}
		);
	}

});
